﻿using Aoc2021Solutions;
using Aoc2021Solutions.Solutions;

string day = "01";

string projectDir = Path.GetFullPath(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"..\..\..\.."));

ISolution solver = Activator.CreateInstance(Type.GetType($"Aoc2021Solutions.Solutions.Solution{day}, Aoc2021Solutions")) as ISolution;
string input = File.ReadAllText(Path.Combine(projectDir, $@"inputs\input{day}.txt"));

Console.WriteLine($"Day {day} solutions.");
string partAAnswer = solver.PartA(input);
string partBAnswer = solver.PartB(input);
Console.WriteLine($"Part A: {partAAnswer}");
Console.WriteLine($"Part B: {partBAnswer}");

_ = Console.ReadKey();