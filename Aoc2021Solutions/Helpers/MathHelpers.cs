﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aoc2021Solutions.Helpers
{
    internal static class MathHelpers
    {
        internal static void IncrementFrequencyDictionary<T>(this Dictionary<T, int> freqDict, T key, int incrementValue)
        {
            if (key is null)
            {
                throw new ArgumentNullException(nameof(key));
            }
            else if (freqDict is null)
            {
                throw new ArgumentNullException(nameof(freqDict));
            }

            freqDict[key] = freqDict.GetValueOrDefault(key, 0) + incrementValue;
        }

        internal static void IncrementFrequencyDictionary<T>(this Dictionary<T, long> freqDict, T key, long incrementValue)
        {
            if (key is null)
            {
                throw new ArgumentNullException(nameof(key));
            }
            else if (freqDict is null)
            {
                throw new ArgumentNullException(nameof(freqDict));
            }

            freqDict[key] = freqDict.GetValueOrDefault(key, 0) + incrementValue;
        }

        /// <summary>
        /// <para>A naive, non-optimal, but very simple to understand method that calculates 
        /// the median value of a collection of integers.</para>
        /// <para>As the median can have a fractional part, the returned type is <see langword="decimal"/>.</para>
        /// </summary>
        /// <returns></returns>
        internal static decimal Median(IEnumerable<int> numbers)
        {
            List<int> numbersCopy = numbers.OrderBy(n => n).ToList();

            int middleIndex = numbersCopy.Count / 2;
            decimal median;
 
            if (numbersCopy.Count % 2 == 0)
            {
                median = (numbersCopy[middleIndex] + numbersCopy[middleIndex - 1]) * 0.5M;
            }
            else
            {
                median = numbersCopy[middleIndex];
            }

            return median;
        }

        internal static decimal Median(IEnumerable<long> numbers)
        {
            List<long> numbersCopy = numbers.OrderBy(n => n).ToList();

            int middleIndex = numbersCopy.Count / 2;
            decimal median;
 
            if (numbersCopy.Count % 2 == 0)
            {
                median = (numbersCopy[middleIndex] + numbersCopy[middleIndex - 1]) * 0.5M;
            }
            else
            {
                median = numbersCopy[middleIndex];
            }

            return median;
        }

    }
}
