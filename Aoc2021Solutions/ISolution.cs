﻿namespace Aoc2021Solutions;

public interface ISolution
{
    public string PartA(string input);
    public string PartB(string input);
    public Task<string> PartAAsync(string input);
    public Task<string> PartBAsync(string input);
}
