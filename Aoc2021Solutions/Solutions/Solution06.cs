﻿namespace Aoc2021Solutions.Solutions;

public class Solution06 : SolutionCommon
{
    private const int SimulationTimeA = 80;
    private const int SimulationTimeB = 256;
    private long PartCommon(string input, int simulationTime)
    {
        Dictionary<int, long> lanternfishFrequencyDictionary =
            input.Split(',')
                 .GroupBy(str => Int32.Parse(str))
                 .ToDictionary(grp => grp.Key, grp => (long)grp.Count());

        for (int i = 0; i < simulationTime; i++)
        {
            Dictionary<int, long> nextDay = new Dictionary<int, long>();
            foreach ((int timer, long count) in lanternfishFrequencyDictionary)
            {
                switch (timer)
                {
                    case int t when t >= 1 && t <= 8:
                        nextDay.IncrementFrequencyDictionary(timer - 1, count);
                        break;
                    case 0:
                        nextDay.IncrementFrequencyDictionary(6, count);
                        nextDay.IncrementFrequencyDictionary(8, count);
                        break;
                }
            }
            lanternfishFrequencyDictionary = nextDay;
        }

        return lanternfishFrequencyDictionary.Values.Sum();
    }

    public override string PartA(string input) => PartCommon(input, SimulationTimeA).ToString();
    public override string PartB(string input) => PartCommon(input, SimulationTimeB).ToString();
}
