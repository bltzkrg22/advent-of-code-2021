namespace Aoc2021Solutions.Solutions;

public class Solution15 : SolutionCommon
{
    private const int WrapThreshold = 9;
    private const int PartBRepeat = 5;

    private static readonly List<(int, int)> Directions = new List<(int, int)>()
    {
        (1,0), (0,1), (-1,0), (0,-1)
    };

    private static int[,] CreateMapA(string input)
    {
        string[] inputLines = input.Split(new[] { "\r\n", "\r", "\n" },
            StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);

        int rows = inputLines.Length;
        int columns = inputLines[0].Length;

        var map = new int[columns, rows];

        for (int y = 0; y < rows; y++)
        {
            for (int x = 0; x < columns; x++)
            {
                map[x, y] = (int)Char.GetNumericValue(inputLines[y][x]);
            }
        }

        return map;
    }

    private static int[,] CreateMapB(string input)
    {
        string[] inputLines = input.Split(new[] { "\r\n", "\r", "\n" },
            StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);

        int rows = inputLines.Length;
        int columns = inputLines[0].Length;

        var map = new int[columns * PartBRepeat, rows * PartBRepeat];

        for (int y = 0; y < rows; y++)
        {
            for (int x = 0; x < columns; x++)
            {
                for (int rx = 0; rx < PartBRepeat; rx++)
                {
                    for (int ry = 0; ry < PartBRepeat; ry++)
                    {
                        map[x + rx * rows, y + ry * rows] = IncrementAndWrap((int)Char.GetNumericValue(inputLines[y][x]), rx + ry, WrapThreshold);
                    }
                }
            }
        }

        return map;
    }

    private static Dictionary<(int x, int y), int> GetNeighbourDistances((int x, int y) point, int[,] map)
    {
        int columns = map.GetLength(0);
        int rows = map.GetLength(1);

        Dictionary<(int x, int y), int> neighbours = new Dictionary<(int x, int y), int>();

        foreach ((int dx, int dy) in Directions)
        {
            int newX = point.x + dx;
            int newY = point.y + dy;
            if (newX >= 0 && newX <= columns - 1 && newY >= 0 && newY <= rows - 1)
            {
                neighbours[(newX, newY)] = map[newX, newY];
            }
        }

        return neighbours;
    }

    private static int IncrementAndWrap(int start, int increment, int wrapValue)
    {
        int newValue = start + increment;
        while (newValue > wrapValue)
        {
            newValue -= wrapValue;
        }
        return newValue;
    }

    public int PartCommon(string input, Func<string, int[,]> createMap)
    {
        int[,] map = createMap(input);
        int columns = map.GetLength(0);
        int rows = map.GetLength(1);

        (int x, int y) finalPoint = (columns - 1, rows - 1);

        Dictionary<(int x, int y), int> distanceFromStart = new Dictionary<(int x, int y), int>();

        PriorityQueue<(int x, int y), int> nextPointQueue = new PriorityQueue<(int x, int y), int>();
        nextPointQueue.Enqueue((0, 0), 0);

        bool endReached = false;
        while (endReached is false)
        {
            _ = nextPointQueue.TryDequeue(out (int x, int y) nextPoint, out int nextPointDistance);
            if (nextPoint == finalPoint)
            {
                distanceFromStart[nextPoint] = nextPointDistance;
                endReached = true;
            }
            else if (distanceFromStart.ContainsKey(nextPoint) is false)
            {
                distanceFromStart[nextPoint] = nextPointDistance;

                Dictionary<(int x, int y), int> neighbourDistances = GetNeighbourDistances(nextPoint, map);
                foreach (((int x, int y) neighbour, int distanceToNeighbour) in neighbourDistances)
                {
                    nextPointQueue.Enqueue(neighbour, nextPointDistance + distanceToNeighbour);
                }
            }
        }

        int endDistance = distanceFromStart[(columns - 1, rows - 1)];
        return endDistance;
    }

    public override string PartA(string input) => PartCommon(input, CreateMapA).ToString();
    public override string PartB(string input) => PartCommon(input, CreateMapB).ToString();
}

