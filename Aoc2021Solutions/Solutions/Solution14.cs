namespace Aoc2021Solutions.Solutions;

public class Solution14 : SolutionCommon
{
    private const int SimulationTimeA = 10;
    private const int SimulationTimeB = 40;

    private static Dictionary<string, long> ParseStartingPolymer(string polymer)
    {
        Dictionary<string, long> pairFrequencyDictionary = new Dictionary<string, long>();

        for (int i = 0; i < polymer.Length - 1; i++)
        {
            char first = polymer[i];
            char second = polymer[i + 1];

            pairFrequencyDictionary.IncrementFrequencyDictionary($"{first}{second}", 1);
        }

        return pairFrequencyDictionary;
    }

    private static Dictionary<string, string> ParsePolymerInsertionRules(string rules)
    {
        string[] ruleLines = rules.Split(new[] { "\r\n", "\r", "\n" },
            StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);

        Dictionary<string, string> rulesDictionary = new Dictionary<string, string>();

        foreach (string rule in ruleLines)
        {
            Regex pattern = new Regex(pattern: @"(\w\w) -> (\w)");
            Match match = pattern.Match(rule);
            if (match.Success is true)
            {
                string pair = match.Groups[1].Value;
                string insert = match.Groups[2].Value;
                rulesDictionary.Add(pair, insert);
            }
            else
            {
                string message = $"Unparsable input line found:{Environment.NewLine}{rule}";
                throw new ApplicationException(message);
            }
        }

        return rulesDictionary;
    }

    public long PartCommon(string input, int simulationTime)
    {
        string[] split = input.Split(new[] { "\r\n\r\n", "\r\r", "\n\n" },
            StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);

        char firstElement = split[0][0];
        char lastElement = split[0][^1];

        Dictionary<string, long> pairFrequencyDictionary = ParseStartingPolymer(split[0]);
        Dictionary<string, string> rulesDictionary = ParsePolymerInsertionRules(split[1]);

        for (int i = 0; i < simulationTime; i++)
        {
            Dictionary<string, long> nextFrequencyDictionary = new Dictionary<string, long>();
            foreach ((string pair, string insert) in rulesDictionary)
            {
                long freq = pairFrequencyDictionary.GetValueOrDefault(pair);

                pairFrequencyDictionary.IncrementFrequencyDictionary(pair, -freq);

                nextFrequencyDictionary.IncrementFrequencyDictionary($"{pair[0]}{insert}", freq);
                nextFrequencyDictionary.IncrementFrequencyDictionary($"{insert}{pair[1]}", freq);
            }

            // If there exists an element pair that is not covered by the insertion rules,
            // it is carried over to the next turn, as nothing is inserted inbetween.
            foreach ((string pair, long freq) in pairFrequencyDictionary.Where(kvp => kvp.Value > 0))
            {
                nextFrequencyDictionary.IncrementFrequencyDictionary(pair, freq);
            }

            pairFrequencyDictionary = nextFrequencyDictionary;
        }

        Dictionary<char, long> elementFrequencyDictionary = new Dictionary<char, long>();

        // Each element *with the exception of the first and the last one* is included in exactly two pairs.
        // So we sum the elements in all pairs, and divide by two.
        foreach ((string pair, long freq) in pairFrequencyDictionary)
        {
            elementFrequencyDictionary.IncrementFrequencyDictionary(pair[0], freq);
            elementFrequencyDictionary.IncrementFrequencyDictionary(pair[1], freq);
        }
        // Remember to handle the edge cases of the first and last elements!
        elementFrequencyDictionary.IncrementFrequencyDictionary(firstElement, 1);
        elementFrequencyDictionary.IncrementFrequencyDictionary(lastElement, 1);
        foreach (var element in elementFrequencyDictionary.Keys)
        {
            elementFrequencyDictionary[element] /= 2;
        }

        long highestFrequency = elementFrequencyDictionary.Values.Max();
        long lowestFrequency = elementFrequencyDictionary.Values.Min();

        return highestFrequency - lowestFrequency;
    }

    public override string PartA(string input) => PartCommon(input, SimulationTimeA).ToString();
    public override string PartB(string input) => PartCommon(input, SimulationTimeB).ToString();
}