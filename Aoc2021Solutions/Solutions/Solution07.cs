namespace Aoc2021Solutions.Solutions;

public class Solution07 : SolutionCommon
{
    private static int FuelRequirementA(int startPoint, int endPoint) 
        => Math.Abs(startPoint - endPoint);

    private static int FuelRequirementB(int startPoint, int endPoint)
    {
        int difference = Math.Abs(startPoint - endPoint);
        return difference * (difference + 1) / 2;
    }
    public override string PartA(string input)
    {
        List<int> crabCoordinates = input.Split(',').Select(crab => Int32.Parse(crab)).ToList();

        // https://en.wikipedia.org/wiki/Geometric_median
        // For a 1D scenario, the geometric median = the “regular” median
        decimal median = Median(crabCoordinates);

        // The result must be a grid point, i.e. an integer.
        // If the median would have a fractional part, there should be two soultions:
        // ceiling and floor of the median.
        int medianFloor = (int)median;

        int fuelRequred = crabCoordinates.Sum(crab => FuelRequirementA(crab, medianFloor));
        return fuelRequred.ToString();
    }

    public override string PartB(string input)
    {
        // BRUTE FORCE :(
        List<int> crabCoordinates = input.Split(',').Select(crab => Int32.Parse(crab)).ToList();

        int minCoordinate = crabCoordinates.Min();
        int maxCoordinate = crabCoordinates.Max();

        int optimalFuelRequired = Int32.MaxValue;

        for (int coord = minCoordinate; coord <= maxCoordinate; coord++)
        {
            int fuelRequred = crabCoordinates.Sum(crab => FuelRequirementB(crab, coord));
            optimalFuelRequired = Math.Min(optimalFuelRequired, fuelRequred);
        }

        return optimalFuelRequired.ToString();
    }


}
