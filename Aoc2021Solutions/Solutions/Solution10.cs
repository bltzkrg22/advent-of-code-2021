namespace Aoc2021Solutions.Solutions;

public class Solution10 : SolutionCommon
{
    private static readonly Dictionary<char, char> _openingToClosing = new Dictionary<char, char>()
    {
        ['('] = ')',
        ['['] = ']',
        ['{'] = '}',
        ['<'] = '>'
    };
    
    private static readonly Dictionary<char, char> _closingToOpening = new Dictionary<char, char>()
    {
        [')'] = '(',
        [']'] = '[',
        ['}'] = '{',
        ['>'] = '<'
    };

    private static readonly Dictionary<char, int> _closingToScoreA = new Dictionary<char, int>()
    {
        [')'] = 3,
        [']'] = 57,
        ['}'] = 1197,
        ['>'] = 25137
    };

    private static readonly Dictionary<char, int> _openingToScoreB = new Dictionary<char, int>()
    {
        ['('] = 1,
        ['['] = 2,
        ['{'] = 3,
        ['<'] = 4
    };

    private static long BracketAutocompleteScore(Stack<char> stack)
    {
        long score = 0;
        while(stack.Count > 0)
        {
            char bracket = stack.Pop();
            score = score * 5 + _openingToScoreB[bracket];
        }
        return score;
    }

    public override string PartA(string input)
    {
        string[] inputLines = input.Split(new[] { "\r\n", "\r", "\n" },
            StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);

        int errorScore = 0;

        foreach (string line in inputLines)
        {
            var openingBracketStack = new Stack<char>();
            foreach (char bracket in line)
            {
                if (_openingToClosing.ContainsKey(bracket))
                {
                    openingBracketStack.Push(bracket);
                }
                else if (openingBracketStack.Count is 0 || openingBracketStack.Pop() != _closingToOpening[bracket])
                {
                    errorScore += _closingToScoreA[bracket];
                }
            }
        }

        return errorScore.ToString();
    }

    public override string PartB(string input)
    {
        string[] inputLines = input.Split(new[] { "\r\n", "\r", "\n" },
            StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);

        List<long> scores = new List<long>();
        foreach (string line in inputLines)
        {
            bool errorLine = false;
            var openingBracketStack = new Stack<char>();
            foreach (char bracket in line)
            {
                if (_openingToClosing.ContainsKey(bracket))
                {
                    openingBracketStack.Push(bracket);
                }
                else if (openingBracketStack.Count is 0 || openingBracketStack.Pop() != _closingToOpening[bracket])
                {
                    errorLine = true;
                    break;
                }
            }

            if (errorLine is false)
            {
                scores.Add(BracketAutocompleteScore(openingBracketStack));
            }
        }

        return ((long)Median(scores)).ToString();
    }
}