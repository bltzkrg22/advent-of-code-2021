namespace Aoc2021Solutions.Solutions;

public class Solution13 : SolutionCommon
{
    private static (HashSet<(int x, int y)>, List<(char axis, int coord)>) ParseInput(string input)
    {
        string[] split = input.Split(new[] { "\r\n\r\n", "\r\r", "\n\n" },
            StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);

        string[] points = split[0].Split(new[] { "\r\n", "\r", "\n" },
            StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);

        string[] folds = split[1].Split(new[] { "\r\n", "\r", "\n" },
            StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);

        HashSet<(int x, int y)> pointSet = new HashSet<(int x, int y)>();
        foreach (string line in points)
        {
            Regex pattern = new Regex(pattern: @"(\d+),(\d+)");
            Match match = pattern.Match(line);
            if (match.Success is true)
            {
                int x = Int32.Parse(match.Groups[1].Value);
                int y = Int32.Parse(match.Groups[2].Value);
                pointSet.Add((x, y));
            }
            else
            {
                string message = $"Unparsable input line found:{Environment.NewLine}{line}";
                throw new ApplicationException(message);
            }
        }

        List<(char axis, int coord)> foldList = new List<(char axis, int coord)>();
        foreach (string line in folds)
        {
            Regex pattern = new Regex(pattern: @"fold along ([xy])=(\d+)");
            Match match = pattern.Match(line);
            if (match.Success is true)
            {
                char axis = match.Groups[1].Value[0];
                int coord = Int32.Parse(match.Groups[2].Value);
                foldList.Add((axis, coord));
            }
            else
            {
                string message = $"Unparsable input line found:{Environment.NewLine}{line}";
                throw new ApplicationException(message);
            }
        }

        return (pointSet, foldList);
    }

    private static (int x, int y) CalculateNewPoint((int x, int y) point, (char axis, int coord) fold)
    {
        // By default, we assume that the original point lies in the part of the paper
        // that is not “transformed”.
        (int x, int y) newPoint = point;

        switch (fold.axis)
        {
            case 'x':
                if (point.x < fold.coord)
                {
                    break;
                }
                else if (point.x > fold.coord)
                {
                    int distance = point.x - fold.coord;
                    newPoint = (fold.coord - distance, point.y);
                    break;
                }
                else
                {
                    string message = "Point lies on a fold line. Undefined behavior.";
                    throw new ApplicationException(message);
                }
            case 'y':
                if (point.y < fold.coord)
                {
                    break;
                }
                else if (point.y > fold.coord)
                {
                    int distance = point.y - fold.coord;
                    newPoint = (point.x, fold.coord - distance);
                    break;
                }
                else
                {
                    string message = "Point lies on a fold line. Undefined behavior.";
                    throw new ApplicationException(message);
                }
            default:
                string message1 = "The default case in the switch expression should be unreachable.";
                throw new ApplicationException(message1);
        }

        return newPoint;
    }

    public override string PartA(string input)
    {
        (HashSet<(int x, int y)> pointSet, List<(char axis, int coord)> foldList) = ParseInput(input);

        var firstFold = foldList[0];
        HashSet<(int x, int y)> afterFirstFold = new HashSet<(int x, int y)>();
        foreach (var point in pointSet)
        {
            afterFirstFold.Add(CalculateNewPoint(point, firstFold));
        }

        return afterFirstFold.Count.ToString();
    }

    public override string PartB(string input)
    {
        (HashSet<(int x, int y)> pointSet, List<(char axis, int coord)> foldList) = ParseInput(input);

        foreach (var fold in foldList)
        {
            HashSet<(int x, int y)> nextPointSet = new HashSet<(int x, int y)>();
            foreach (var point in pointSet)
            {
                nextPointSet.Add(CalculateNewPoint(point, fold));
            }
            pointSet = nextPointSet;
        }

        int finalRowNumber = foldList.Where(f => f.axis is 'y').MinBy(f => f.coord).coord;
        int finalColumnNumber = foldList.Where(f => f.axis is 'x').MinBy(f => f.coord).coord;

        var origami = new StringBuilder(Environment.NewLine);
        for (int y = 0; y < finalRowNumber; y++)
        {
            for (int x = 0; x < finalColumnNumber; x++)
            {
                char @char = pointSet.Contains((x, y)) ? '#' : ' ';
                origami.Append(@char);
            }
            origami.Append(Environment.NewLine);
        }
        origami.Remove(origami.Length - 1, 1);

        return origami.ToString();
    }
}