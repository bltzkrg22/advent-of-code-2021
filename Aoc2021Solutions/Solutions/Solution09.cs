namespace Aoc2021Solutions.Solutions;

public class Solution09 : SolutionCommon
{
    private static readonly List<(int, int)> Directions = new List<(int, int)>()
    {
        (1,0), (0,1), (-1,0), (0,-1)
    };

    private int _rows;
    private int _columns;

    private List<List<char>> _heightmap;

    private char GetHeightAtCoordinate(int x, int y) => _heightmap[y + 1][x + 1];

    private void ParseInput(string input)
    {
        _heightmap = new List<List<char>>();
        string[] inputLines = input.Split(new[] { "\r\n", "\r", "\n" },
            StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);

        _rows = inputLines.Length;
        _columns = inputLines[0].Length;

        // To simplify checking for the neighbours at edges, we surround whole data matrix
        // with sentinel values. In this scenario, '9' is a valid sentinel.
        _heightmap.Add(Enumerable.Repeat('9', _columns + 2).ToList());
        foreach (string line in inputLines)
        {
            List<char> row = new List<char>();
            row.Add('9');
            row.AddRange(line);
            row.Add('9');

            _heightmap.Add(row);
        }
        _heightmap.Add(Enumerable.Repeat('9', _columns + 2).ToList());
    }

    private List<char> GetNeighbourHeights(int x, int y)
    {
        List<char> neighbours = new List<char>();

        foreach ((int dx, int dy) in Directions)
        {
            neighbours.Add(GetHeightAtCoordinate(x + dx, y + dy));
        }

        return neighbours;
    }
    
    private int GetRiskLevel(int x, int y)
    {
        char height = GetHeightAtCoordinate(x, y);
        List<char> neighbours = GetNeighbourHeights(x, y);
        if (neighbours.All(x => x > height))
        {
            return (height - '0') + 1;
        }
        else
        {
            return 0;
        }
    }

    private List<(int x, int y)> GetNotNineNeighbours(int x, int y)
    {
        List<(int x, int y)> neighbours = new List<(int x, int y)>();

        foreach ((int dx, int dy) in Directions)
        {
            if (GetHeightAtCoordinate(x + dx, y + dy) < '9')
            {
                neighbours.Add((x + dx, y + dy));
            }
        }

        return neighbours;
    }

    private List<(int x, int y)> GetNotNineNeighbours((int x, int y) point)
        => GetNotNineNeighbours(point.x, point.y);

    public override string PartA(string input)
    {
        ParseInput(input);

        int sum = 0;
        for (int y = 0; y < _rows; y++)
        {
            for (int x = 0; x < _columns; x++)
            {
                sum += GetRiskLevel(x, y);
            }
        }

        return sum.ToString();
    }

    public override string PartB(string input)
    {
        ParseInput(input);

        HashSet<(int x, int y)> pointsAlreadyChecked = new HashSet<(int x, int y)>();
        // The `basin origin` is not the lowest point of each basin,
        // but actually the first point of the basin that is encoutered 
        // by the doubly-nested for loop!
        Dictionary<(int x, int y), (int x, int y)> pointToBasinOrigin = new Dictionary<(int x, int y), (int x, int y)>();

        for (int y = 0; y < _rows; y++)
        {
            for (int x = 0; x < _columns; x++)
            {
                if (GetHeightAtCoordinate(x, y) < '9' && pointsAlreadyChecked.Contains((x, y)) is false)
                {
                    (int, int) origin = (x, y);
                    var thisBasinQueue = new Queue<(int x, int y)>();
                    thisBasinQueue.Enqueue(origin);

                    while (thisBasinQueue.Count > 0)
                    {
                        (int, int) point = thisBasinQueue.Dequeue();
                        if (pointsAlreadyChecked.Contains(point) is false)
                        {
                            pointsAlreadyChecked.Add(point);
                            pointToBasinOrigin[point] = origin;

                            foreach (var neighbour in GetNotNineNeighbours(point))
                            {
                                thisBasinQueue.Enqueue(neighbour);
                            }
                        }
                    }
                }
            }
        }

        int productOfThreeLargest = pointToBasinOrigin.GroupBy(kvp => kvp.Value)
                                                      .Select(grp => grp.Count())
                                                      .OrderByDescending(count => count)
                                                      .Take(3)
                                                      .Aggregate(1, (a, b) => a * b); // multiplication

        return productOfThreeLargest.ToString();
    }
}