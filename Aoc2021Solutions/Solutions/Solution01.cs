﻿namespace Aoc2021Solutions.Solutions;

public class Solution01 : SolutionCommon
{
    public override string PartA(string input)
    {
        string[] inputLines = input.Split(new[] { "\r\n", "\r", "\n" },
            StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);

        int counter = 0;
        int previousNumber = Int32.Parse(inputLines[0]);
        for (int i = 1; i < inputLines.Length; i++)
        {
            int currentNumber = Int32.Parse(inputLines[i]);
            if (currentNumber > previousNumber)
            {
                counter++;
            }
            previousNumber = currentNumber;
        }

        return counter.ToString();
    }

    public override string PartB(string input)
    {
        string[] inputLines = input.Split(new[] { "\r\n", "\r", "\n" },
            StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);

        int counter = 0;
        int previousWindowSum = Int32.Parse(inputLines[0]) + Int32.Parse(inputLines[1]) + Int32.Parse(inputLines[2]);
        for (int i = 3; i < inputLines.Length; i++)
        {
            int incomingNumber = Int32.Parse(inputLines[i]);
            int outgoingNumber = Int32.Parse(inputLines[i-3]);
            int currentWindowSum = previousWindowSum + incomingNumber - outgoingNumber;
            if (currentWindowSum > previousWindowSum)
            {
                counter++;
            }
            previousWindowSum = currentWindowSum;
        }

        return counter.ToString();
    }
}