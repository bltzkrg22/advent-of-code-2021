namespace Aoc2021Solutions.Solutions;

public class Solution08 : SolutionCommon
{
    private static readonly HashSet<char> _fullSegmentset = new HashSet<char>() { 'a', 'b', 'c', 'd', 'e', 'f', 'g' };
    private static readonly HashSet<int> PartADigits = new HashSet<int>() { 1, 4, 7, 8 };

    private (List<HashSet<char>> tenDigits, List<string> fourDisplays) ParseInputLine(string inputLine)
    {
        string[] split = inputLine.Split("|", StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);
        List<HashSet<char>> tenDigits = split[0].Split(" ", StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries)
            .Select(str => new HashSet<char>(str))
            .ToList();
        List<string> fourDisplays = split[1].Split(" ", StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries)
            .ToList();

        return (tenDigits, fourDisplays);
    }

    /// <summary>
    /// <para>Method that calculates the mapping of each segment char from the input line, to
    /// appropriate segment char from the normal seven segment display.</para>
    /// <para>Example: if 'a' is mapped to 'g', then in this particular input line 'a'
    /// lights up the bottom segment.</para>
    /// </summary>
    /// <param name="tenDigits"></param>
    /// <returns></returns>
    private Dictionary<char, char> SolveWirings(List<HashSet<char>> tenDigits)
    {
        Dictionary<char, char> mapping = new Dictionary<char, char>();

        // seven = HashSet of (unmapped) segments that are used to display digit 7
        #region Top segment (a)
        HashSet<char> seven = tenDigits.Single(set => set.Count == 3);
        HashSet<char> one = tenDigits.Single(set => set.Count == 2);
        char segmentA = seven.Except(one).Single();
        mapping[segmentA] = 'a';
        #endregion Top segment (a)

        // sixSegmentsCommon = HashSet of four segments, that are common for all digits
        // that are made up from six segments, i.e. 0, 6 & 9
        #region Right segments (c,f)
        List<HashSet<char>> sixSegmentsCommonList = tenDigits.FindAll(set => set.Count == 6);
        HashSet<char> sixSegmentsCommon = new HashSet<char>(sixSegmentsCommonList[0]);
        sixSegmentsCommon.IntersectWith(sixSegmentsCommonList[1]);
        sixSegmentsCommon.IntersectWith(sixSegmentsCommonList[2]);
        char segmentF = one.Intersect(sixSegmentsCommon).Single();
        mapping[segmentF] = 'f';

        char segmentC = one.Where(@char => @char != segmentF).Single();
        mapping[segmentC] = 'c';
        #endregion Right segments (c,f)

        // fiveSegmentsCommon = HashSet of three segments, that are common for all digits
        // that are made up from five segments, i.e. 2, 3 & 5
        #region Middle segment (d)
        List<HashSet<char>> fiveSegmentsCommonList = tenDigits.FindAll(set => set.Count == 5);
        HashSet<char> fiveSegmentsCommon = new HashSet<char>(fiveSegmentsCommonList[0]);
        fiveSegmentsCommon.IntersectWith(fiveSegmentsCommonList[1]);
        fiveSegmentsCommon.IntersectWith(fiveSegmentsCommonList[2]);
        char segmentD = fiveSegmentsCommon.Except(sixSegmentsCommon).Single();
        mapping[segmentD] = 'd';
        #endregion Middle segment (d)

        #region Top-left segment (b)
        HashSet<char> four = tenDigits.Single(set => set.Count == 4);
        char segmentB = four.Except(new char[] { segmentC, segmentD, segmentF }).Single();
        mapping[segmentB] = 'b';
        #endregion Top-left segment (b)

        #region Bottom segment (g)
        char segmentG = fiveSegmentsCommon.Except(new char[] { segmentA, segmentD }).Single();
        mapping[segmentG] = 'g';
        #endregion Bottom segment (g)

        #region Bottom-left segment (e)
        char segmentE = _fullSegmentset.Except(new char[] { segmentA, segmentB, segmentC, segmentD, segmentF, segmentG }).Single();
        mapping[segmentE] = 'e';
        #endregion Bottom segment (e)

        return mapping;
    }

    private int DecodeDigit(string displayedSegments, Dictionary<char, char> mapping)
    {
        string sortedSegments = String.Concat(displayedSegments.Select(@char => mapping[@char]).OrderBy(@char => @char));
        return sortedSegments switch
        {
            "abcefg" => 0,
            "cf" => 1,
            "acdeg" => 2,
            "acdfg" => 3,
            "bcdf" => 4,
            "abdfg" => 5,
            "abdefg" => 6,
            "acf" => 7,
            "abcdefg" => 8,
            "abcdfg" => 9,
            _ => throw new ApplicationException("Impossible to decode with given mapping."),
        };
    }

    public override string PartA(string input)
    {
        string[] inputLines = input.Split(new[] { "\r\n", "\r", "\n" },
            StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);

        int count = 0;

        foreach (var line in inputLines)
        {
            (List<HashSet<char>> tenDigits, List<string> fourDisplays) = ParseInputLine(line);
            Dictionary<char, char> mapping = SolveWirings(tenDigits);
            foreach (string display in fourDisplays)
            {
                int digit = DecodeDigit(display, mapping);
                if (PartADigits.Contains(digit))
                {
                    count++;
                }
            }

        }

        return count.ToString();
    }

    public override string PartB(string input)
    {
        string[] inputLines = input.Split(new[] { "\r\n", "\r", "\n" },
            StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);

        int sum = 0;

        foreach (var line in inputLines)
        {
            int subSum = 0;
            (List<HashSet<char>> tenDigits, List<string> fourDisplays) = ParseInputLine(line);
            Dictionary<char, char> mapping = SolveWirings(tenDigits);

            subSum += 1000 * DecodeDigit(fourDisplays[0], mapping);
            subSum += 100 * DecodeDigit(fourDisplays[1], mapping);
            subSum += 10 * DecodeDigit(fourDisplays[2], mapping);
            subSum += DecodeDigit(fourDisplays[3], mapping);

            sum += subSum;
        }

        return sum.ToString();
    }
}
