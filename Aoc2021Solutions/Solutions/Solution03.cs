namespace Aoc2021Solutions.Solutions;

public class Solution03 : SolutionCommon
{
    public override string PartA(string input)
    {
        string[] inputLines = input.Split(new[] { "\r\n", "\r", "\n" },
            StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);

        int lineLenght = inputLines[0].Length;

        int[] zeroesFrequency = new int[lineLenght];
        int[] onesFrequency = new int[lineLenght];

        foreach (string line in inputLines)
        {
            for (int i = 0; i < lineLenght; i++)
            {
                switch (line[i])
                {
                    case '0':
                        zeroesFrequency[i] += 1;
                        break;
                    case '1':
                        onesFrequency[i] += 1;
                        break;
                }
            }
        }

        // gamma = more common bit, epsilon = least common bit
        int gamma = 0;
        int epsilon = 0;

        int power = 1;
        for (int i = 1; i <= lineLenght; i++)
        {
            if (onesFrequency[^i] > zeroesFrequency[^i])
            {
                gamma += power;
            }
            else if (onesFrequency[^i] < zeroesFrequency[^i])
            {
                epsilon += power;
            }
            else
            {
                const string message = "Behavior is undefined when there is an equal number of ones and zeroes.";
                throw new ApplicationException(message);
            }

            power <<= 1;
        }

        long answer = (long)gamma * (long)epsilon;
        return answer.ToString();
    }

    public override string PartB(string input)
    {
        string[] inputLines = input.Split(new[] { "\r\n", "\r", "\n" },
                    StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);
        int lineLenght = inputLines[0].Length;
        int index;

        // Oxygen generator
        List<string> oxygenLines = inputLines.ToList();
        index = 0;
        while (oxygenLines.Count > 1 && index < lineLenght)
        {
            List<string> nextIterationLines = new List<string>();
            int zeroFrequency = 0;
            int oneFrequency = 0;
            foreach (string line in oxygenLines)
            {
                switch (line[index])
                {
                    case '0':
                        zeroFrequency++;
                        break;
                    case '1':
                        oneFrequency++;
                        break;
                }
            }

            char moreCommonBit = oneFrequency >= zeroFrequency ? '1' : '0';

            foreach (string line in oxygenLines)
            {
                if (line[index] == moreCommonBit)
                {
                    nextIterationLines.Add(line);
                }
            }

            oxygenLines = nextIterationLines;
            index++;
        }

        string oxygenString;
        if (oxygenLines.Count is 1)
        {
            oxygenString = oxygenLines[0];
        }
        else
        {
            const string message = "A single line is expected at the end of operation.";
            throw new ApplicationException(message);
        }

        // CO2 scrubber
        List<string> co2Lines = inputLines.ToList();
        index = 0;
        while (co2Lines.Count > 1 && index < lineLenght)
        {
            List<string> nextIterationLines = new List<string>();
            int zeroFrequency = 0;
            int oneFrequency = 0;
            foreach (string line in co2Lines)
            {
                switch (line[index])
                {
                    case '0':
                        zeroFrequency++;
                        break;
                    case '1':
                        oneFrequency++;
                        break;
                }
            }

            char leastCommonBit = zeroFrequency <= oneFrequency ? '0' : '1';

            foreach (string line in co2Lines)
            {
                if (line[index] == leastCommonBit)
                {
                    nextIterationLines.Add(line);
                }
            }

            co2Lines = nextIterationLines;
            index++;
        }

        string co2String;
        if (co2Lines.Count is 1)
        {
            co2String = co2Lines[0];
        }
        else
        {
            const string message = "A single line is expected at the end of operation.";
            throw new ApplicationException(message);
        }

        long oxygenGeneratorRating = Convert.ToInt64(value: oxygenString, fromBase: 2);
        long co2ScrubberRating = Convert.ToInt64(value: co2String, fromBase: 2);
        long answer = oxygenGeneratorRating * co2ScrubberRating;
        return answer.ToString();
    }
}
