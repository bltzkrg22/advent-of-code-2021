namespace Aoc2021Solutions.Solutions;

public class Solution05 : SolutionCommon
{
    private static ((int, int), (int, int)) ParseInputLine(string line)
    {
        Regex pattern = new Regex(pattern: @"(\d+),(\d+) -> (\d+),(\d+)");
        Match match = pattern.Match(line);
        if (match.Success is true)
        {
            int a = Int32.Parse(match.Groups[1].Value);
            int b = Int32.Parse(match.Groups[2].Value);
            int c = Int32.Parse(match.Groups[3].Value);
            int d = Int32.Parse(match.Groups[4].Value);
            return ((a, b), (c, d));
        }
        else
        {
            string message = $"Unparsable input line found:{Environment.NewLine}{line}";
            throw new ApplicationException(message);
        }
    }
    public override string PartA(string input)
    {
        string[] inputLines = input.Split(new[] { "\r\n", "\r", "\n" },
            StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);

        // Key = (x,y) coordinates
        // Value = count of lines that overlap the (x,y) coordinate
        var heatmap = new Dictionary<(int, int), int>();

        foreach (string line in inputLines)
        {
            ((int startX, int startY), (int endX, int endY)) = ParseInputLine(line);

            // If line is horizontal, increment every point from startX to endX
            if (startY == endY)
            {
                for (int i = Math.Min(startX, endX); i <= Math.Max(startX, endX); i++)
                {
                    heatmap[(i, startY)] = heatmap.GetValueOrDefault((i, startY), defaultValue: 0) + 1;
                }
            }
            // If line is vertical, increment every point from startX to endX
            else if (startX == endX)
            {
                for (int i = Math.Min(startY, endY); i <= Math.Max(startY, endY); i++)
                {
                    heatmap[(startX, i)] = heatmap.GetValueOrDefault((startX, i), defaultValue: 0) + 1;
                }
            }
        }

        int answer = heatmap.Count(kvp => kvp.Value >= 2);
        return answer.ToString();
    }

    public override string PartB(string input)
    {
        string[] inputLines = input.Split(new[] { "\r\n", "\r", "\n" },
            StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);

        // Key = (x,y) coordinates
        // Value = count of lines that overlap the (x,y) coordinate
        var heatmap = new Dictionary<(int, int), int>();

        foreach (string line in inputLines)
        {
            ((int startX, int startY), (int endX, int endY)) = ParseInputLine(line);

            // If line is horizontal, increment every point from startX to endX
            if (startY == endY)
            {
                for (int i = Math.Min(startX, endX); i <= Math.Max(startX, endX); i++)
                {
                    heatmap[(i, startY)] = heatmap.GetValueOrDefault((i, startY), defaultValue: 0) + 1;
                }
            }
            // If line is vertical, increment every point from startY to endY
            else if (startX == endX)
            {
                for (int i = Math.Min(startY, endY); i <= Math.Max(startY, endY); i++)
                {
                    heatmap[(startX, i)] = heatmap.GetValueOrDefault((startX, i), defaultValue: 0) + 1;
                }
            }

            // If line is diagonal, increment all points analogously
            else if (Math.Abs(endX - startX) == Math.Abs(endY - startY))
            {
                int stepX = Math.Sign(endX - startX);
                int stepY = Math.Sign(endY - startY);

                for (int i = 0; i <= Math.Abs(endX - startX); i++)
                {
                    heatmap[(startX + i * stepX, startY + i * stepY)] =
                        heatmap.GetValueOrDefault((startX + i * stepX, startY + i * stepY), defaultValue: 0) + 1;
                }
            }
        }

        int answer = heatmap.Count(kvp => kvp.Value >= 2);
        return answer.ToString();
    }
}