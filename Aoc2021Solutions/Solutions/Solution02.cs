﻿namespace Aoc2021Solutions.Solutions;

public class Solution02 : SolutionCommon
{
    public class Submarine
    {
        public int HorizontalPosition { get; private set; }
        public int Depth { get; private set; }
        public int Aim { get; private set; }

        public Submarine(int horizontalPosition, int depth, int aim = 0)
        {
            HorizontalPosition = horizontalPosition;
            Depth = depth;
            Aim = aim;
        }


        public void MoveA(string command)
        {
            string[] split = command.Split(' ');
            string direction = split[0];
            int value = Int32.Parse(split[1]);

            switch (direction)
            {
                case "forward":
                    HorizontalPosition += value;
                    break;
                case "down":
                    Depth += value;
                    break;
                case "up":
                    Depth -= value;
                    break;
            }
        }

        public void MoveB(string command)
        {
            string[] split = command.Split(' ');
            string direction = split[0];
            int value = Int32.Parse(split[1]);

            switch (direction)
            {
                case "forward":
                    HorizontalPosition += value;
                    Depth += Aim * value;
                    break;
                case "down":
                    Aim += value;
                    break;
                case "up":
                    Aim -= value;
                    break;
            }
        }
    }

    public override string PartA(string input)
    {
        var submarine = new Submarine(horizontalPosition: 0, depth: 0);

        var reader = new StringReader(input);
        string? line;
        while ((line = reader.ReadLine()) is not null)
        {
            submarine.MoveA(line);
        }

        long answer = (long)submarine.HorizontalPosition * submarine.Depth;
        return answer.ToString();
    }

    public override string PartB(string input)
    {
        var submarine = new Submarine(horizontalPosition: 0, depth: 0, aim: 0);

        var reader = new StringReader(input);
        string? line;
        while ((line = reader.ReadLine()) is not null)
        {
            submarine.MoveB(line);
        }

        long answer = (long)submarine.HorizontalPosition * submarine.Depth;
        return answer.ToString();
    }
}
