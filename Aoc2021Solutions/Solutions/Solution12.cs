namespace Aoc2021Solutions.Solutions;

public class Solution12 : SolutionCommon
{
    private const string StartNode = "start";
    private const string EndNode = "end";

    private static Dictionary<string, HashSet<string>> ParseInput(string input)
    {
        string[] inputLines = input.Split(new[] { "\r\n", "\r", "\n" },
            StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);

        Dictionary<string, HashSet<string>> map = new Dictionary<string, HashSet<string>>();

        foreach (string line in inputLines)
        {
            Regex pattern = new Regex(pattern: @"(\w+)-(\w+)");
            Match match = pattern.Match(line);
            if (match.Success is true)
            {
                string cavernA = match.Groups[1].Value;
                string cavernB = match.Groups[2].Value;

                AddPathToMap(map, cavernA, cavernB);
            }
            else
            {
                string message = $"Unparsable input line found:{Environment.NewLine}{line}";
                throw new ApplicationException(message);
            }
        }

        return map;
    }

    private static void AddPathToMap(Dictionary<string, HashSet<string>> map, string nodeA, string nodeB)
    {
        if (map.ContainsKey(nodeA) is false)
        {
            map[nodeA] = new HashSet<string>() { nodeB };
        }
        else
        {
            map[nodeA].Add(nodeB);
        }

        if (map.ContainsKey(nodeB) is false)
        {
            map[nodeB] = new HashSet<string>() { nodeA };
        }
        else
        {
            map[nodeB].Add(nodeA);
        }
    }

    private static bool ValidityConditionA(Stack<string> path, string neighbour)
        => Char.IsUpper(neighbour[0]) || (Char.IsLower(neighbour[0]) && path.Contains(neighbour) is false);

    private static bool ValidityConditionB(Stack<string> path, string neighbour)
    {
        if (neighbour is StartNode) // start can be visited only once
        {
            return false;
        }
        else if (Char.IsUpper(neighbour[0])) // big cave can be visited any number of times
        {
            return true;
        }
        else if (path.Contains(neighbour) is false) // each small cave can be always visited once
        {
            return true;
        }
        // if this is the first time a small cave was visited twice, it is also a valid path
        else if (path.Count(cave => cave == neighbour) <= 1 && SomeSmallCaveAlreadyVisitedTwice(path) is false)
        {
            return true;
        }
        else
        {
            return false;
        }

        static bool SomeSmallCaveAlreadyVisitedTwice(Stack<string> path)
            => path.Where(cave => Char.IsLower(cave[0]))
                   .GroupBy(cave => cave)
                   .MaxBy(grp => grp.Count())
                   .Count() >= 2;
    }

    private void RecursiveSearchRoutine(
        Stack<string> path,
        Dictionary<string, HashSet<string>> map,
        ref HashSet<string> validPaths,
        Func<Stack<string>, string, bool> validityCondition)
    {
        string topNode = path.Peek();

        if (topNode is EndNode)
        {
            string validPath = String.Join(",", Enumerable.Reverse(path));
            validPaths.Add(validPath);
            return;
        }

        HashSet<string> neighbours = map[topNode];

        foreach (var neighbour in neighbours)
        {
            // We only need to check the first letter to determine cavern size
            if (validityCondition(path, neighbour))
            {
                var newPath = new Stack<string>(Enumerable.Reverse(path));
                newPath.Push(neighbour);
                RecursiveSearchRoutine(newPath, map, ref validPaths, validityCondition);
            }
        }
    }

    public int PartCommon(string input, Func<Stack<string>, string, bool> validityCondition)
    {
        Dictionary<string, HashSet<string>> cavernMap = ParseInput(input);
        HashSet<string> foundPaths = new HashSet<string>();

        Stack<string> path = new Stack<string>();
        path.Push(StartNode);

        RecursiveSearchRoutine(path, cavernMap, ref foundPaths, validityCondition);

        return foundPaths.Count;
    }

    public override string PartA(string input) => PartCommon(input, ValidityConditionA).ToString();
    public override string PartB(string input) => PartCommon(input, ValidityConditionB).ToString();
}