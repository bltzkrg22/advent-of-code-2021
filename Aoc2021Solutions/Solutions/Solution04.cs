namespace Aoc2021Solutions.Solutions;

public class Solution04 : SolutionCommon
{
    private const int GridSize = 5;

    internal class BingoBoard
    {
        private readonly int[,] _numbers = new int[GridSize, GridSize];
        private readonly bool[,] _markedOut = new bool[GridSize, GridSize];

        internal BingoBoard(string constructionString)
        {
            string[] rows = constructionString.Split(new[] { "\r\n", "\r", "\n" },
                StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);

            for (int j = 0; j < GridSize; j++)
            {
                string[] numbersInRow = rows[j].Split(" ", StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);
                for (int i = 0; i < GridSize; i++)
                {
                    _numbers[i, j] = Int32.Parse(numbersInRow[i]);
                }
            }
        }

        internal int SumOfUnmarked()
        {
            int sum = 0;
            for (int i = 0; i < GridSize; i++)
            {
                for (int j = 0; j < GridSize; j++)
                {
                    if (_markedOut[i, j] is false)
                    {
                        sum += _numbers[i, j];
                    }
                }
            }
            return sum;
        }

        internal bool CrossDrawnNumber(int number)
        {
            for (int i = 0; i < GridSize; i++)
            {
                for (int j = 0; j < GridSize; j++)
                {
                    if (_numbers[i, j] == number)
                    {
                        _markedOut[i, j] = true;
                        if (CheckForBingo(i, j) is true)
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        private bool CheckForBingo(int x, int y)
        {
            HashSet<bool> row = new HashSet<bool>() { _markedOut[0,y], _markedOut[1,y],
                _markedOut[2,y], _markedOut[3,y], _markedOut[4,y] };

            if (row.Contains(false) is false)
            {
                return true;
            }

            HashSet<bool> column = new HashSet<bool>() { _markedOut[x,0], _markedOut[x,1],
                _markedOut[x,2], _markedOut[x,3], _markedOut[x,4] };

            if (column.Contains(false) is false)
            {
                return true;
            }

            return false;
        }
    }

    private static (List<int> numbers, List<BingoBoard> boards) ParseInput(string input)
    {
        string[] split = input.Split(new[] { "\r\n\r\n", "\r\r", "\n\n" },
            StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);

        List<int> numbers = split[0].Split(',').Select(x => Int32.Parse(x)).ToList();

        List<BingoBoard> boards = new List<BingoBoard>();
        for (int i = 1; i < split.Length; i++)
        {
            boards.Add(new BingoBoard(constructionString: split[i]));
        }

        return (numbers, boards);
    }

    public override string PartA(string input)
    {
        List<int> numbers;
        List<BingoBoard> boards;
        (numbers, boards) = ParseInput(input);

        int index = 0;
        bool bingo = false;
        while (bingo is false && index < numbers.Count)
        {
            int nextNumber = numbers[index];
            foreach (var board in boards)
            {
                bingo = board.CrossDrawnNumber(nextNumber);
                if (bingo is true)
                {
                    int answer = nextNumber * board.SumOfUnmarked();
                    return answer.ToString();
                }
            }

            index++;
        }

        const string message = "There was no winning board!";
        throw new ApplicationException(message);
    }


    public override string PartB(string input)
    {
        List<int> numbers;
        List<BingoBoard> boards;
        (numbers, boards) = ParseInput(input);

        int index = 0;
        while (boards.Count > 1 && index < numbers.Count)
        {
            int nextNumber = numbers[index];
            boards.RemoveAll(b => b.CrossDrawnNumber(nextNumber) is true);
            index++;
        }

        if (boards.Count is 0)
        {
            const string message = "There are multiple boards that are tied in the last place.";
            throw new ApplicationException(message);
        }
        else if (boards.Count is 1)
        {
            // Keep playing until the last board wins, to get the score.
            int nextNumber = default;
            bool bingo = false;
            while (bingo is false)
            {
                nextNumber = numbers[index];
                bingo = boards[0].CrossDrawnNumber(nextNumber);
                index++;
            }

            int answer = nextNumber * boards[0].SumOfUnmarked();
            return answer.ToString();
        }
        else
        {
            const string message = "There are multiple boards that don’t win at all.";
            throw new ApplicationException(message);
        }
    }
}
