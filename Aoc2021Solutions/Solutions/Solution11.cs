namespace Aoc2021Solutions.Solutions;

public class Solution11 : SolutionCommon
{
    private const int SimulationTimeA = 100;
    private const int FlashThreshold = 9;
    private const int RowSize = 10;
    private const int ColumnSize = 10;

    private static readonly List<(int, int)> Directions = new List<(int, int)>()
    {
        (1,0), (1,1), (0,1), (-1,1), (-1,0), (-1,-1), (0,-1), (1,-1)
    };

    private int[,] ParseInput(string input)
    {
        int[,] energyLevels = new int[ColumnSize, RowSize];

        string[] inputLines = input.Split(new[] { "\r\n", "\r", "\n" },
            StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);

        for (int y = 0; y < RowSize; y++)
        {
            for (int x = 0; x < ColumnSize; x++)
            {
                energyLevels[x, y] = (int)Char.GetNumericValue(inputLines[y][x]);
            }
        }

        return energyLevels;
    }

    private static List<(int x, int y)> GetNeighbours(int x, int y)
    {
        List<(int x, int y)> neighbours = new List<(int x, int y)>();

        foreach ((int dx, int dy) in Directions)
        {
            int newX = x + dx;
            int newY = y + dy;
            if (newX >= 0 && newX <= ColumnSize - 1 && newY >= 0 && newY <= RowSize - 1)
            {
                neighbours.Add((x + dx, y + dy));
            }
        }

        return neighbours;
    }

    private static List<(int x, int y)> GetNeighbours((int x, int y) point)
        => GetNeighbours(point.x, point.y);

    private void IncrementEnergyLevels(int[,] energyLevels)
    {
        for (int y = 0; y < RowSize; y++)
        {
            for (int x = 0; x < ColumnSize; x++)
            {
                energyLevels[x, y] += 1;
            }
        }
    }

    private int FlashAndIncrement(int[,] energyLevels)
    {
        var alreadyFlashed = new HashSet<(int x, int y)>();
        var nextToFlash = new Queue<(int x, int y)>();

        for (int y = 0; y < RowSize; y++)
        {
            for (int x = 0; x < ColumnSize; x++)
            {
                if (energyLevels[x, y] > FlashThreshold)
                {
                    nextToFlash.Enqueue((x, y));
                }
            }
        }

        int count = 0;

        while (nextToFlash.Count > 0)
        {
            var octopus = nextToFlash.Dequeue();

            if (alreadyFlashed.Contains(octopus) is false)
            {
                count++;
                alreadyFlashed.Add(octopus);

                foreach (var neighbour in GetNeighbours(octopus))
                {
                    energyLevels[neighbour.x, neighbour.y] += 1;
                    if (energyLevels[neighbour.x, neighbour.y] > FlashThreshold)
                    {
                        nextToFlash.Enqueue(neighbour);
                    }
                }
            }
        }

        return count;
    }

    private void ResetAfterFlash(int[,] energyLevels)
    {
        for (int y = 0; y < RowSize; y++)
        {
            for (int x = 0; x < ColumnSize; x++)
            {
                if (energyLevels[x, y] > FlashThreshold)
                {
                    energyLevels[x, y] = 0;
                }
            }
        }
    }


    public override string PartA(string input)
    {
        int[,] energyLevels = ParseInput(input);

        int count = 0;

        for (int time = 0; time < SimulationTimeA; time++)
        {
            IncrementEnergyLevels(energyLevels);

            count += FlashAndIncrement(energyLevels);

            ResetAfterFlash(energyLevels);
        }

        return count.ToString();
    }

    public override string PartB(string input)
    {
        int[,] energyLevels = ParseInput(input);

        int time = 0;
        int simultaneousFlashCount;

        while (true)
        {
            time++;

            IncrementEnergyLevels(energyLevels);

            simultaneousFlashCount = FlashAndIncrement(energyLevels);
            if (simultaneousFlashCount >= RowSize * ColumnSize)
            {
                break;
            }

            ResetAfterFlash(energyLevels);
        }

        return time.ToString();
    }
}