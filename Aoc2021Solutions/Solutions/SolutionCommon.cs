﻿namespace Aoc2021Solutions.Solutions;

public abstract class SolutionCommon : ISolution
{
    private const string NoSolutionYet = @"Error. The solution has not been implemented yet.";

    public virtual string PartA(string input) => NoSolutionYet;

    public virtual string PartB(string input) => NoSolutionYet;

    public virtual Task<string> PartAAsync(string input) => Task.FromResult(PartA(input));

    public virtual Task<string> PartBAsync(string input) => Task.FromResult(PartB(input));
}